package net.poundex.glreqspex;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface SatisfiesGitlabRequirement {
    /**
     * @return the "IID" of the Gitlab Requirement
     */
    String value();
}
