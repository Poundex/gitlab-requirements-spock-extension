package net.poundex.glreqspex;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.Tuple;
import io.vavr.collection.Stream;
import org.spockframework.runtime.AbstractRunListener;
import org.spockframework.runtime.extension.IGlobalExtension;
import org.spockframework.runtime.model.ErrorInfo;
import org.spockframework.runtime.model.FeatureInfo;
import org.spockframework.runtime.model.SpecInfo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class GitlabRequirementsExtension extends AbstractRunListener implements IGlobalExtension {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final Map<FeatureInfo, SatisfiesGitlabRequirement> featureToGlRequirement = new ConcurrentHashMap<>();
    private final Map<SatisfiesGitlabRequirement, Result> glRequirementToResult = new ConcurrentHashMap<>();

    @Override
    public void start() { }

    @Override
    public void visitSpec(SpecInfo spec) {
        featureToGlRequirement.putAll(Stream.ofAll(spec.getAllFeatures())
                .map(f -> Tuple.of(f, f.getFeatureMethod().getAnnotation(SatisfiesGitlabRequirement.class)))
                .filter(f -> f._2() != null)
                .toJavaMap(f -> f));

        spec.addListener(this);
    }

    @Override
    public void stop() {
        try {
            writeResults();
        } catch (final Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void writeResults() throws IOException {
        objectMapper.writeValue(Files.newBufferedWriter(Paths.get("build/requirements.json")),
                featureToGlRequirement.entrySet().stream()
                        .collect(Collectors.toMap(
                                e -> e.getValue().value(),
                                e -> glRequirementToResult.getOrDefault(e.getValue(), Results.PASSED).toGitlabString())));
    }

    @Override
    public void error(ErrorInfo error) {
        Optional.ofNullable(featureToGlRequirement.get(error.getMethod().getFeature()))
                .ifPresent(req -> glRequirementToResult.put(req, Results.FAILED));
    }

    interface Result {
        String toGitlabString();
    }

    enum Results implements Result {
        PASSED("passed"),
        FAILED("failed");

        private final String gitlabString;

        Results(String gitlabString) {
            this.gitlabString = gitlabString;
        }

        @Override
        public String toGitlabString() {
            return gitlabString;
        }
    }
}
